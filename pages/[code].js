import React from "react";
import Layout from "../components/layout/Layout";
import ProductDetails from "../components/ProductDetails";
import { genuka_api_2021_10 } from "../utils/configs";

function ProductByCode({ product, company }) {
  return (
    <Layout
      company={company}
      head={
        <>
          <title>{product.name + " - " + company.name}</title>
          <link rel="favicon" href={company.logo ? company.logo : ""} />
          <link rel="icon" href={company.logo ? company.logo : ""} />
          <meta name="description" content={product?.description?.replace(/<[^>]*>?/gm, "")} />
          <meta
            name="keywords"
            content={product?.description
              ?.replace(/<[^>]*>?/gm, "")
              .split(" ")
              .join(", ")}
          />
          <meta name="author" content={product.name} />
          <meta name="robots" content="index, follow" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta name="apple-mobile-web-app-status-bar-style" content="black" />
          <meta name="apple-mobile-web-app-title" content={product.name} />
          <meta name="msapplication-TileColor" content="#222" />
          <meta name="msapplication-TileImage" content={product.medias.length > 0 ? product.medias[0].link : company.logo ? company.logo : ""} />
          <meta name="theme-color" content="#222" />
          <meta property="og:title" content={product.name} />
          <meta property="og:description" content={product?.description?.replace(/<[^>]*>?/gm, "")} />
          <meta property="og:image" content={product.medias.length > 0 ? product.medias[0].link : company.logo ? company.logo : ""} />
          <meta property="og:type" content="product" />
          <meta property="og:site_name" content={product.name} />
        </>
      }
    >
      <ProductDetails company={company} product={product} />
    </Layout>
  );
}

export async function getServerSideProps(context) {
  let company, product, result;
  const { query } = context;
  const { code: code_ou_slug } = query;
  try {
    result = await fetch(`${genuka_api_2021_10}/products/code/${code_ou_slug}`);
    product = await result.json();
  } catch (error) {
    try {
      result = await fetch(`${genuka_api_2021_10}/products/slug/${code_ou_slug}`);
      product = await result.json();
    } catch (e) {
      return {
        redirect: {
          permanent: false,
          destination: "/404",
        },
        props: {},
      };
    }
  }
  
  if (product && product.company_id) {
    result = await fetch(`${genuka_api_2021_10}/companies/details/${product.company_id}`);
    company = await result.json();
    return {
      props: {
        company,
        product,
      }, // will be passed to the page component as props
    };
  } else {
    return {
      redirect: {
        permanent: false,
        destination: "/404",
      },
      props: {},
    };
  }

  
}

export default ProductByCode;
