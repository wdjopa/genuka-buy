import { Button, Center, Container, Divider, Image, Link, Text, VStack } from "@chakra-ui/react";
import { Player } from "@lottiefiles/react-lottie-player";

/* eslint-disable react/no-unescaped-entities */
function Custom404({}) {
  return (
    <Container maxW="xl">
      <Center height="100vh">
        <VStack>
          <Image src="https://genuka.com/assets/img/logo-two.png" alt="Logo de Genuka" height="12" />
          <br />
          <br />
          <Player autoplay loop src="/assets/404.json" style={{ width: "50vh" }} />
          <Text fontSize="4xl" textAlign={"center"}>
            Cette page produit n'existe pas.
          </Text>
          <br />
          <br />
          <br />
          <Text fontSize="xl" textAlign={"center"} paddingBottom="2vh">
            Commencez à vendre en ligne vous aussi avec Genuka <strong>gratuitement</strong>
          </Text>
          <Link href={"https://genuka.com/?from=404"} mt={"5vh"} isExternal _hover={{ textDecoration: "none" }}>
            <Button color="white" bg="#FFAB01CC" _hover={{ background: "#FFAB01CC" }} size="lg">
              <Image src="https://dashboard.genuka.com/logo.png" boxSize="20px" alt="Boutton Genuka" />
              &nbsp;&nbsp;Vendez en ligne grâce à Genuka
            </Button>
          </Link>
          <br />
          <br />
          <Divider />
          <br />
          <Text fontSize={"xl"}>
            Tous droits réservés{" "}
            <Link color="orange" href="https://genuka.com/">
              Genuka
            </Link>{" "}
            2022.
          </Text>
        </VStack>
      </Center>
    </Container>
  );
}

export default Custom404;
