/* eslint-disable react/no-unescaped-entities */
import { Link, Text } from '@chakra-ui/react';

export default function Home() {
  return (
    <Text>
      Si vous n'êtes pas automatiquement redirigé cliquez <Link href="https://genuka.com/?from=buy.genuka.com">ici</Link>
    </Text>
  );
}

export async function getServerSideProps() {
  return {
    redirect: {
      permanent: true,
      destination: "https://genuka.com/?from=buy.genuka.com",
    },
    props: {},
  };
}
