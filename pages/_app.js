import { ChakraProvider } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Loading from "../components/common/Loading";
import { GenukaProvider } from "../store/genukaStore";
import "../styles/globals.css";
import NextNProgress from "nextjs-progressbar";

function MyApp({ Component, pageProps }) {

  return (
    <GenukaProvider>
      <ChakraProvider>
        <NextNProgress />
        <Component {...pageProps} />
      </ChakraProvider>
    </GenukaProvider>
  );
}

export default MyApp;
