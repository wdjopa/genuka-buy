/* eslint-disable react/no-unescaped-entities */
import { Alert, AlertDescription, AlertIcon, AlertTitle, Button, Container, Divider, Drawer, DrawerBody, DrawerCloseButton, DrawerContent, DrawerFooter, DrawerHeader, DrawerOverlay, Link, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useDisclosure, VStack } from "@chakra-ui/react";
import { Player } from "@lottiefiles/react-lottie-player";
import React, { useEffect } from "react";
import { cancelMyOrder, loadOrder, useGenukaDispatch, useGenukaState } from "../store/genukaStore";
import Error from "./common/Error";
import PaymentModal from "./PaymentModal";
import PaymentProcessMobileMoney from "./PaymentProcessMobileMoney";

function OrderStatusPage() {
  const { current_order: order, order_loaded, orders, company, error } = useGenukaState();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { isOpen: openConfirm, onOpen: onOpenConfirm, onClose: onCloseConfirm } = useDisclosure();
  const { isOpen: openArchiveConfirm, onOpen: onOpenArchiveConfirm, onClose: onCloseArchiveConfirm } = useDisclosure();
  const btnRef = React.useRef();
  const dispatch = useGenukaDispatch();
  useEffect(() => {
    if (order) {
      if (!order_loaded) {
        loadOrder(dispatch, order);
      }
      onOpen();
    }
  }, [order]);

  const cancelOrder = (order) => {
    // Les clients peuvent annuler leur commande tant que le paiement n'est pas effectué
    cancelMyOrder(dispatch, order);
  };

  if (!order) {
    return <></>;
  }
  return (
    <>
      {orders.map((order) => (
        <Button
          key={Math.random()}
          ref={btnRef}
          onClick={() => {
            onOpen();
            dispatch({ type: "current_order", payload: order });
          }}
          size="md"
          height="50px"
          width="100%"
          colorScheme={"yellow"}
        >
          SUIVRE LA COMMANDE {order.reference}
        </Button>
      ))}
      {orders.length > 0 && <Divider my="5" />}
      <Drawer isOpen={isOpen} size="lg" placement="bottom" onClose={onClose}>
        <DrawerOverlay />
        <DrawerContent>
          <Container maxW="2xl" maxH="100vh" style={{ overflow: "auto" }}>
            <DrawerCloseButton />
            <DrawerHeader>Commande {order.reference}</DrawerHeader>
            <DrawerBody>
              <div>
                <Text fontSize={"lg"}>
                  ✅ Commande passée (
                  <Text fontSize={"md"} display="inline" as="b">
                    {new Intl.NumberFormat("en-US", {
                      style: "currency",
                      currency: company.currency.code,
                      maximumFractionDigits: 0,
                    }).format(order.total)}
                  </Text>
                  )
                </Text>
                <Text fontSize={"sm"}>
                  Vous avez commandé
                  {order.products &&
                    order.products.map((product) => {
                      return (
                        <span key={Math.random()}>
                          {product.quantity} {product.name}
                        </span>
                      );
                    })}
                  <br />
                  le {new Date(order.created_at).toLocaleString()}
                </Text>
              </div>
              <Divider my="3" />
              <div>
                {order.payment_state === 0 ? (
                  <>
                    <Text fontSize={"lg"}>
                      ⌛ Paiement en attente par <strong>{company.payment_modes ? company.payment_modes[order.payment_mode].full_name : order.payment_mode}</strong>
                    </Text>
                    <Player autoplay loop src="/assets/payment_process_animation.json" style={{ height: "300px", width: "300px" }} />
                    {order && ["mobilemoney", "momo"].includes(order.payment_mode) && <PaymentProcessMobileMoney order={order} />}
                    {order && ["paypal", "card"].includes(order.payment_mode) && <PaymentModal order={order} company={company} />}
                  </>
                ) : (
                  <>
                    <Text fontSize={"lg"}>
                      ✅ Paiement effectué par <strong>{company.payment_modes ? company.payment_modes[order.payment_mode].full_name : order.payment_mode}</strong>
                    </Text>
                    {/* <Player autoplay loop src="/assets/check.json" style={{ height: "300px", width: "300px" }} /> */}
                  </>
                )}
              </div>
              <Divider my="3" />
              <div>
                {order.shipping_state === 0 ? (
                  <>
                    {order.payment_state === 0 && <Text fontSize={"lg"}>❌ Le traitement de la commande aura lieu après le paiement</Text>}
                    {order.payment_state === 1 && <Text fontSize={"lg"}>⌛ Commande en cours de préparation</Text>}
                    {order.payment_state === 1 && <Player autoplay loop src="/assets/loading.json" style={{ height: "300px", width: "300px" }} />}
                  </>
                ) : (
                  <>
                    <Text fontSize={"lg"}>✅ Commande terminée</Text>
                  </>
                )}
              </div>
              {order.payment_state === 1 && order.shipping_state === 1 && <Player autoplay loop src="/assets/check.json" style={{ height: "300px", width: "300px" }} />}
            </DrawerBody>
            <DrawerFooter>
              <VStack>
                {error && <Error error_text={error} />}
                {order.payment_state === 1 && order.shipping_state === 1 && (
                  <Button
                    colorScheme="whatsapp"
                    width="full"
                    onClick={() => {
                      onOpenArchiveConfirm();
                    }}
                  >
                    Marquer la commande comme terminée 👍🏾
                  </Button>
                )}

                <Button colorScheme="yellow" width="full" mt="5">
                  <Link href={company.phone ? "tel:" + company.phone : "email:" + company.email}>📞 Contacter {company.name}</Link>
                </Button>
                <Divider m="2" />
                {order.payment_state === 0 ? (
                  <Button
                    colorScheme="red"
                    width="full"
                    onClick={() => {
                      onOpenConfirm();
                    }}
                  >
                    Annuler la commande
                  </Button>
                ) : (
                  <></>
                )}
                <Button
                  width="full"
                  onClick={() => {
                    onClose();
                  }}
                >
                  Retour
                </Button>
                <Modal isCentered isOpen={openArchiveConfirm} onClose={onCloseArchiveConfirm} size={"xs"}>
                  <ModalOverlay bg="blackAlpha.300" backdropFilter="blur(10px) hue-rotate(90deg)" />{" "}
                  <ModalContent>
                    <ModalHeader>Archivage de la commande</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                      <Text>Si vous êtes là, alors votre commande s'est bien déroulée. Merci d'avoir fait confiance à {company.name} 😊.</Text>
                    </ModalBody>
                    <ModalFooter>
                      <Button
                        size="lg"
                        colorScheme="whatsapp"
                        width="full"
                        onClick={() => {
                          dispatch({
                            type: "orders",
                            payload: orders.filter((o) => o.id !== order.id),
                          });
                          onClose();
                          onCloseArchiveConfirm();
                        }}
                      >
                        Marquer comme terminée
                      </Button>
                    </ModalFooter>
                  </ModalContent>
                </Modal>

                <Modal isCentered isOpen={openConfirm} onClose={onCloseConfirm} size={"xs"}>
                  <ModalOverlay bg="blackAlpha.300" backdropFilter="blur(10px) hue-rotate(90deg)" />{" "}
                  <ModalContent>
                    <ModalHeader>Annulation de la commande</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                      <Text>Souhaitez vous vraiment annuler cette commande ? Cette action est irrévocable !</Text>
                    </ModalBody>
                    <ModalFooter>
                      <Button
                        size="lg"
                        colorScheme="red"
                        width="full"
                        onClick={() => {
                          cancelOrder(order);
                          onCloseConfirm();
                        }}
                      >
                        Oui, annuler
                      </Button>
                    </ModalFooter>
                  </ModalContent>
                </Modal>
                <small>Tant que le paiement n'est pas effectué, vous pouvez annuler votre commande. Une fois le paiement effectué, veuillez vous rapprocher des services de {order.entreprise?.nom}</small>
              </VStack>
            </DrawerFooter>
          </Container>
        </DrawerContent>
      </Drawer>
    </>
  );
}

export default OrderStatusPage;
