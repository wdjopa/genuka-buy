import { Center } from '@chakra-ui/react'
import React from 'react'

function Loading({loading}) {
  return (
    <Center>
        {loading ? 'Chargement du produit' : 'produit chargé'}
    </Center>
  )
}

export default Loading