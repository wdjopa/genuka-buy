import { Alert, AlertDescription, AlertIcon, AlertTitle, CloseButton, useDisclosure } from "@chakra-ui/react";
import React from "react";
import { useGenukaDispatch } from "../../store/genukaStore";

function Error({ error_text }) {
    const dispatch = useGenukaDispatch()
  const { isOpen: isVisible, onClose, onOpen } = useDisclosure({ defaultIsOpen: true });
  return (
    <Alert status="error">
      <AlertIcon />
      {/* <AlertTitle>Error!</AlertTitle> */}
      <AlertDescription>{error_text}</AlertDescription>
      <CloseButton alignSelf="flex-start" position="relative" right={-1} top={-1} onClick={() => {
        dispatch({type: "error", payload: undefined})
        onClose()
      }} />
    </Alert>
  );
}

export default Error;
