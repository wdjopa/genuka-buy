/* eslint-disable react/no-unescaped-entities */
import { Avatar } from "@chakra-ui/avatar";
import { Button } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import { Box, Divider, Flex, HStack, Text } from "@chakra-ui/layout";
import { Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay } from "@chakra-ui/modal";
import React from "react";
import ReviewModal from "./ReviewModal";

function ReviewCard({ review, product, deleteReview, canEditOrDelete, ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const review_date = new Date(review.created_at).toLocaleDateString("fr-FR", { weekday: "long", year: "numeric", month: "short", day: "numeric", hour: "2-digit", minute: "2-digit" });
  return (
    <>
      <Box p={5} shadow="md" borderWidth="1px" {...rest} bgColor="whiteAlpha.900" my="4">
        <HStack>
          <Avatar size="sm" name={review.customer.first_name + " " + review.customer.last_name} src={review.customer.profile_picture} />
          <Text fontSize="xl">
            {review.customer.first_name} {review.customer?.last_name[0]}. <br />
            <span style={{ fontSize: "1rem" }}> {review.note} ⭐</span>
          </Text>
        </HStack>
        <Text mt={4} fontSize="lg">
          {review.message}
        </Text>
        <Divider my="2" />
        <Flex justifyContent={"flex-end"} alignItems="center">
          {canEditOrDelete ? <>
            <Text mx="2" fontSize="xs" textAlign={"right"} onClick={onOpen} cursor={"pointer"} _hover={{ color: "red" }}>
              {"Supprimer l'avis"}
            </Text>
            <>-</>
            <Text mx="2" fontSize="xs" textAlign={"right"}>
              <ReviewModal product={product} review={review} />
            </Text>
            <>-</>
          </> : <></>}
          <Text mx="2" fontSize="xs" textAlign={"right"}>
            <span>Avis laissé le {review_date}</span>
          </Text>
        </Flex>
      </Box>
      <Modal isCentered isOpen={isOpen} onClose={onClose} size={"xs"}>
        <ModalOverlay bg="blackAlpha.300" backdropFilter="blur(10px) hue-rotate(90deg)" />{" "}
        <ModalContent>
          <ModalHeader>Suppression d'un avis</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text>Êtes-vous sûr de vouloir supprimer l'avis ci-dessous ?</Text>
            <br />
            <Text>
              <strong>Note</strong> : {review.note}/5 ⭐<br />
              <strong>Message</strong> : {review.message}
              <br />
              <strong>Laissé le</strong> : {review_date}
            </Text>
          </ModalBody>
          <ModalFooter>
            <Button
              size="md"
              onClick={() => {
                onClose();
              }}
            >
              Non, annuler
            </Button>
            <Button
              size="md"
              colorScheme="red"
              onClick={() => {
                deleteReview(review);
                onClose();
              }}
            >
              Oui, supprimer
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default ReviewCard;
