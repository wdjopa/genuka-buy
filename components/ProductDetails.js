import { Box, Button, Center, Container, Divider, Stack, Tab, TabList, TabPanel, TabPanels, Tabs, Text } from "@chakra-ui/react";
import { Player } from "@lottiefiles/react-lottie-player";
import React, { useEffect } from "react";
import { deleteAReview, getUser, useGenukaDispatch, useGenukaState } from "../store/genukaStore";
import BottomBar from "./product/BottomBar";
import MediasBlock from "./product/MediasBlock";
import VariantsBlock from "./product/VariantsBlock";
import ReviewCard from "./ReviewCard";
import ReviewModal from "./ReviewModal";

function ProductDetails({ company, product }) {
  const dispatch = useGenukaDispatch();
  const { productInCart, reviews, user, token } = useGenukaState();

  useEffect(() => {
    if (product && company && (!productInCart.product || productInCart.product.id !== product.id)) {
      dispatch({ type: "hydrate_product", payload: { product, company } });
      dispatch({
        type: "reviews",
        payload: {
          list: product.reviews,
          total_reviews: product.total_reviews,
          avg_reviews: product.avg_reviews,
        },
      });
    }
  }, [product, company, dispatch]);

  useEffect(() => {
    if(!user && token) {
      getUser(dispatch)
    }
  }, [token])

  const clearVariant = ({ variant }) => {
    dispatch({ type: "clear_variant", payload: variant });
  };

  const selectVariantOption = ({ variant, option }) => {
    dispatch({ type: "add_variant_option", payload: { variant, option } });
  };

  return (
    <div className="product-container" style={{ marginBottom: "4rem" }}>
      <Container maxW="2xl">
        <Text fontSize="4xl">{product.name}</Text>
        <Text fontSize={"xs"} mb="3">
          🏭 {company.name} - <strong>Boutique {company.certified ? "vérifiée ✅" : "non vérifiée"}</strong>
        </Text>
        <Center>
          <MediasBlock product={product} />
        </Center>

        <Divider my="2" />
        <VariantsBlock product={product} selectVariantOption={selectVariantOption} productInCart={productInCart} clearVariant={clearVariant} />
        <br />
        <Tabs colorScheme="black">
          <TabList>
            <Tab>Description</Tab>
            <Tab>Avis clients {reviews?.avg_reviews?.toFixed(2)}⭐ </Tab>
          </TabList>

          <TabPanels>
            <TabPanel px="0">
              <div dangerouslySetInnerHTML={{ __html: product.description }} />
            </TabPanel>
            <TabPanel px="0">
              {reviews.list.length === 0 ? (
                <Box>
                  <Player autoplay loop src="/assets/empty-state.json" style={{ height: "300px", width: "300px" }} />
                  <Text textAlign={"center"}>Aucun avis laissé pour le moment</Text>
                  <br />
                  <Stack alignItems={"center"}>
                    <ReviewModal company={company} product={product} />
                  </Stack>
                </Box>
              ) : (
                <>
                  <Stack alignItems={"flex-end"}>
                    <ReviewModal company={company} product={product} />
                  </Stack>
                  <Box>
                    {reviews.list.map((review) => {
                      return (
                      <ReviewCard
                        key={review.id}
                        canEditOrDelete={review.customer && review.customer.id === user?.id}
                        review={review}
                        product={product}
                        deleteReview={(review) => {
                          deleteAReview(dispatch, product, review);
                        }}
                      />
                    )})}
                  </Box>
                </>
              )}
            </TabPanel>
          </TabPanels>
        </Tabs>
        <br />
        <BottomBar
          productInCart={productInCart}
          company={company}
          decreaseQty={() => {
            dispatch({ type: "update_product_qty", payload: -1 });
          }}
          increaseQty={() => {
            dispatch({ type: "update_product_qty", payload: 1 });
          }}
        />
      </Container>
    </div>
  );
}

export default ProductDetails;
