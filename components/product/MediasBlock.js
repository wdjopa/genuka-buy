import { Image, VStack } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";

function MediaReader({ mainMedia }) {
  const [isAnImage, setIsAnImage] = useState(true);
  useEffect(() => {
    if (mainMedia) {
      setIsAnImage(true);
    }
  }, [mainMedia]);

  return isAnImage ? (
    <Image
      bgColor={"#55555511"}
      height="400px"
      width="400px"
      objectFit={"contain"}
      borderRadius={"5px"}
      alt=""
      src={mainMedia}
      onError={(e) => {
        setIsAnImage(false);
      }}
    />
  ) : (
    <video style={{ height: "400px", background: "black", borderRadius: "5px" }} controls autoPlay src={mainMedia} onError={(e) => {}} />
  );
}
function MediasBlock({ product }) {
  const [mainMedia, setMainMedia] = useState();

  useEffect(() => {
    if (product && product.medias.length > 0) {
      setMainMedia(product.medias[0].link);
    }
  }, [product]);
  if (!mainMedia) return <></>;
  return (
    <VStack>
      {<MediaReader mainMedia={mainMedia} />}
      <div className="thumbnails-container">
        {product.medias.map((media, i) => {
          return (
            <div style={{ display: "inline-block" }} key={media.id + i}>
              <Image
                border="2px"
                borderRadius={"4px"}
                bgColor={mainMedia === media.link ? "#EEE":"#55555511"}
                borderColor={mainMedia === media.link ? "black" : "transparent"}
                display={"inline-block"}
                onClick={() => {
                  setMainMedia(media.link);
                }}
                objectFit="contain"
                height="70px"
                width="70px"
                src={media.thumb}
                onError={({ currentTarget }) => {
                  currentTarget.onerror = null; // prevents looping
                  currentTarget.src = "https://via.placeholder.com/70/000000?text=Video";
                }}
                alt={"Image du produit " + product.name}
              />
            </div>
          );
        })}
      </div>
    </VStack>
  );
}

export default MediasBlock;
