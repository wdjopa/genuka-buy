import { Button, ButtonGroup, Container, HStack, IconButton, Text, VStack } from "@chakra-ui/react";
import React from "react";
import CheckoutDrawer from "../CheckoutDrawer";
import Minus from "../icons/Minus";
import Plus from "../icons/Plus";
import OrderStatusPage from "../OrderStatusPage";

function BottomBar({ productInCart, increaseQty, decreaseQty, company }) {
  return (
    <div className="bottom-bar">
      <Container maxW="2xl" >
        <OrderStatusPage />
        <VStack spacing={4}>
          <HStack width="100%" justify={"space-between"}>
            <ButtonGroup size="md" isAttached variant="outline" colorScheme={"orange"}>
              <IconButton aria-label="Remove " icon={<Minus width={"15"} />} onClick={decreaseQty} />
              <Button>{productInCart.quantity}</Button>
              <IconButton aria-label="Add " icon={<Plus width={"15"} height={"15"} />} onClick={increaseQty} />
            </ButtonGroup>
            <Text fontSize={"2xl"}>
              <span style={{ fontWeight: "bold", marginRight: "5px" }}>
                {new Intl.NumberFormat("en-US", {
                  style: "currency",
                  currency: company.currency.code,
                  maximumFractionDigits: 0,
                }).format(productInCart.price)}
              </span>
              {/* <sup>{company.currency.symbol}</sup> */}
            </Text>
          </HStack>

          <CheckoutDrawer />
        </VStack>
      </Container>
    </div>
  );
}

export default BottomBar;
