import { Tag, Text, Tooltip } from "@chakra-ui/react";
import React, { useState } from "react";

function Variant({ variant, selectVariantOption, productInCart, clearVariant }) {
  const [variantIsOpened, setVariantIsOpened] = useState(true);
  return (
    <div style={{ margin: "20px 0" }}>
      <Text
        fontSize={"xl"}
        as="b"
        onClick={() => {
          setVariantIsOpened(!variantIsOpened);
        }}
      >
        <span style={{ transform: "rotate(" + (!variantIsOpened ? 0 : 90) + "deg)", display: "inline-block" }}>▶</span> {variant.name} <span style={{ color: "#dd5f5f" }}>{variant.required ? "[Obligatoire]" : ""}</span>
      </Text>
      <br />

      {!variantIsOpened ? (
        <></>
      ) : (
        <>
          <small>
            {variant.max_choices} choix {variant.max_choices > 1 ? "max" : ""} possible.{" "}
            <Text
              as="u"
              onClick={() => {
                clearVariant({ variant });
              }}
              cursor={"pointer"}
            >
              Annuler tous les choix
            </Text>
          </small>
          <Text>{variant.description}</Text>
          {variant.options.map((option) => {
            const isSelected = productInCart.variants
              .find((v) => v.slug === variant.slug)
              ?.options?.map((o) => o.id)
              ?.includes(option.id);
            return (
              <Tooltip label={option.description} key={Math.random()}>
                <Tag
                  size="lg"
                  onClick={() => {
                    if (!isSelected)
                     selectVariantOption({ variant, option });
                  }}
                  variant="outline"
                  m="1"
                  bg={isSelected ? "black" : "white"}
                  color={isSelected ? "white" : "black"}
                  cursor={"pointer"}
                >
                  {option.name}
                </Tag>
              </Tooltip>
            );
          })}
        </>
      )}
    </div>
  );
}

function VariantsBlock({ product, selectVariantOption, productInCart, clearVariant }) {
  if (product.variants.length === 0) return <></>;
  return (
    <div>
      {product.variants.map((variant, i) => {
        return <Variant key={"variant_" + i} variant={variant} selectVariantOption={selectVariantOption} productInCart={productInCart} clearVariant={clearVariant} />;
      })}
    </div>
  );
}

export default VariantsBlock;
