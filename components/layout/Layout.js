/* eslint-disable react-hooks/exhaustive-deps */
import Head from "next/head";
import React, { useEffect } from "react";
import { useGenukaState } from "../../store/genukaStore";
import Notifications from "../common/Notifications";
import Footer from "./Footer";
import Header from "./Header";

const Layout = ({ company, children, head }) => {
  const { notifications } = useGenukaState();
  const [globalStyle, setGlobalStyle] = React.useState({ "--main-color": "black", "--primary-color": "green", "--secondary-color": "red", "--main-font": "josefin sans" });

  useEffect(() => {
    setGlobalStyle({ ...globalStyle, "--primary-color": "#348989", "--secondary-color": "#D31B51" });
  }, [company]);

  useEffect(() => {
    if (!window.is_google_translated_added) {
      window.is_google_translated_added = true;
      
      let addScriptGA = document.createElement("script");
      addScriptGA.setAttribute("async", "true");
      addScriptGA.setAttribute("src", "https://www.googletagmanager.com/gtag/js?id=G-Q83E2K8TPB");
      document.body.appendChild(addScriptGA);
      
      let addScriptGA2 = document.createElement("script");
      window.dataLayer = window.dataLayer || []; 
      window.dataLayer.push("js", new Date());
      window.dataLayer.push("config", "G-Q83E2K8TPB");
      document.body.appendChild(addScriptGA2);

      let addScript = document.createElement("script");
      addScript.setAttribute("src", "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit");
      document.body.appendChild(addScript);
      window.googleTranslateElementInit = googleTranslateElementInit;
      window.selectLanguage = setInterval(() => {
        if (window.languageSelected) {
          clearInterval(window.selectLanguage);
          return;
        }
        if (!window.navigator?.language?.includes("fr")) {
          let select = document.getElementsByClassName("goog-te-combo")[0];
          if (select) {
            select.selectedIndex = 1;
            select.addEventListener("click", function () {
              select.dispatchEvent(new Event("change"));
            });
            select.click();
            window.languageSelected = true;
          }
        } else {
          window.languageSelected = true;
        }
      }, 1000);
    }
  }, []);

  const googleTranslateElementInit = () => {
    return new window.google.translate.TranslateElement(
      {
        pageLanguage: "fr",
        // layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT,
        layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
        autoDisplay: false,
        includedLanguages: "fr,en",
      },
      "google_translate_element"
    );
  };

  return (
    <div style={globalStyle} className="body">
      <Head>
        {head ? (
          head
        ) : (
          <>
            <title>
              {company.name} - {company.description}
            </title>
            <link rel="favicon" href={company.logo} />
            <link rel="icon" href={company.logo} />
      
          </>
        )}
        
      </Head>
      <Header company={company} />
      <div id="google_translate_element"></div>

      <div className="container">{children}</div>

      <Notifications notifications={notifications} />
      <Footer company={company} />
    </div>
  );
};

export default Layout;
