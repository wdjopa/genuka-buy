import { Avatar, Center, Image, Text } from "@chakra-ui/react";
import React from "react";

function Header({ company }) {
  return (
    <Center h="70px" style={{ marginBottom: "3rem" }}>
      {company.logo ? <Image  h="70px" src={company.logo} alt={company.name} /> : <Text fontSize="5xl">{company.name}</Text>}
    </Center>
  );
}

export default Header;
