import { Avatar, Button, Center, Divider, Image, Link, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useDisclosure, VStack } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import {version} from "../../package.json"
function LegalsModal({ legal, open, close }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  useEffect(() => {
    if (open) {
      onOpen();
    }
  }, [open, onOpen]);

  const closeModal = () => {
    onClose();
    close();
  };
  if (!legal) return <></>;
  return (
    <Modal isOpen={isOpen} onClose={closeModal}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{legal.title}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <div dangerouslySetInnerHTML={{ __html: legal.text }}></div>
        </ModalBody>
        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={closeModal}>
            Close
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
function Footer({ company }) {
  const [isOpen, setIsOpen] = useState(false);
  const [legalPageToShow, setLegalPageToShow] = useState();
  return (
    <div className="footer">
      <Divider />
      <Center my="5">
        <Text>
          Cette page a été générée avec{" "}
          <Link href={"https://genuka.com/?from=" + company.name} isExternal color="#ffab01">
            Genuka
          </Link>{" "}
          V{version}
        </Text>
      </Center>
      <Center>
        <Link href={"https://genuka.com/?from=" + company.name} isExternal _hover={{ textDecoration: "none" }}>
          <Button color="white" bg="#ff8800" _hover={{ background: "#FFAB01CC" }} size="sm">
            <Image src="https://dashboard.genuka.com/logo.png" boxSize="20px" alt="Boutton Genuka" />
            &nbsp;&nbsp;Créez votre page de vente sur Genuka
          </Button>
        </Link>
      </Center>

      {company.legals.length > 0 ? (
        <>
          <Divider my="5" />
          <Center my="3">
            <VStack spacing={4} align="center">
              {company.legals.map((legal) => (
                <Text
                  key={"legals-" + legal.id}
                  onClick={() => {
                    setIsOpen(true);
                    setLegalPageToShow(legal);
                  }}
                >
                  {legal.title}
                </Text>
              ))}
            </VStack>
          </Center>
          <LegalsModal
            legal={legalPageToShow}
            open={isOpen}
            close={() => {
              setIsOpen(false);
              setLegalPageToShow(undefined);
            }}
          />
        </>
      ) : (
        <></>
      )}
    </div>
  );
}

export default Footer;
