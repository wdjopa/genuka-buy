import React from "react";

function Plus(props) {
  return (
    <svg width="17" height="18" viewBox="0 0 17 18" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <line y1="9.5" x2="17" y2="9.5" stroke={props.color || "black"} />
      <line x1="9" y1="17.5" x2="9" y2="0.5" stroke={props.color || "black"} />
    </svg>
  );
}

export default Plus;
