/* eslint-disable react/no-unescaped-entities */
import { Box, Button, Divider, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Slider, SliderFilledTrack, SliderThumb, SliderTrack, Text, Textarea, useDisclosure } from "@chakra-ui/react";
import React, { useEffect, useRef, useState } from "react";
import { sendAReview, updateAReview, useGenukaDispatch, useGenukaState } from "../store/genukaStore";
import Error from "./common/Error";

function ReviewModal({ product, review }) {
  const { loading, error, token } = useGenukaState();
  const dispatch = useGenukaDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [rating, setRating] = useState(5);
  const ref = useRef(null);

  const onSave = () => {
    let message = ref.current.value;
    if (rating && message) {
      sendAReview(dispatch, product, { note: rating, message });
      onClose();
    } else {
      dispatch({ type: "error", payload: "Ajoutez un message pour laisser un avis." });
    }
  };
  const onEdit = () => {
    let message = ref.current.value;
    if (rating && message) {
      updateAReview(dispatch, product, { id: review.id, note: rating, message });
      onClose();
    } else {
      dispatch({ type: "error", payload: "Entrez un message pour enregistrer un avis." });
    }
  };

  useEffect(() => {
    if (localStorage && localStorage.getItem("access_token")) {
      dispatch({ type: "token", payload: localStorage.getItem("access_token") });
    }
  }, []);

  useEffect(() => {
    if (review) {
      setRating(review.note);
    }
  }, [review]);

  return (
    <>
      {token ? (
        !review ? (
          <Button colorScheme="orange" onClick={onOpen}>
            Ajouter un avis
          </Button>
        ) : (
          <span style={{ cursor: "pointer" }} onClick={onOpen}>
            Modifier l'avis
          </span>
        )
      ) : (
        <></>
      )}
      <Modal isCentered isOpen={isOpen} onClose={onClose} size={"sm"}>
        <ModalOverlay bg="blackAlpha.300" backdropFilter="blur(10px) hue-rotate(90deg)" />{" "}
        <ModalContent>
          <ModalHeader>Laisser un avis </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text size="xs">Laissez votre avis pour le produit {product.name}</Text>

            <Divider my="3" />
            <Text size="md">Donnez une note : {rating} / 5 ⭐</Text>
            <br />
            <div style={{ width: "100%" }}>
              <Slider
                min={20}
                aria-label="slider-note"
                defaultValue={rating * 20}
                step={20}
                colorScheme="orange"
                onChange={(value) => {
                  setRating(value / 20);
                }}
              >
                <SliderTrack>
                  <SliderFilledTrack />
                </SliderTrack>
                <SliderThumb bgColor="orange">
                </SliderThumb>
              </Slider>
            </div>
            <br />
            <Textarea placeholder="Ajoutez un message" ref={ref} defaultValue={review?.message} />
            {error && <Error error_text={error} />}
          </ModalBody>
          <ModalFooter>
            <Button mr={3} size={"sm"} onClick={onClose}>
              Annuler
            </Button>
            <Button colorScheme="orange" mr={3} size={"sm"} onClick={review ? onEdit : onSave} disabled={loading && loading.review} loading={loading && loading.review}>
              Valider
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default ReviewModal;
