/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/no-children-prop */
import { Button, Container, Divider, Drawer, DrawerBody, DrawerCloseButton, DrawerContent, DrawerFooter, DrawerHeader, DrawerOverlay, FormControl, HStack, Input, InputGroup, InputLeftAddon, Radio, RadioGroup, Text, Textarea, useDisclosure, VStack } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { placeOrder, useGenukaDispatch, useGenukaState } from "../store/genukaStore";
import { validateEmail, validatePhoneNumber } from "../utils/helpers";
import Error from "./common/Error";
import PaymentModal from "./PaymentModal";

function CheckoutDrawer() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = React.useRef();
  const { company, productInCart, current_order, loading, error } = useGenukaState();
  const dispatch = useGenukaDispatch();
  const [allowToBuy, setAllowToBuy] = useState(false);
  const [fields, setFields] = useState({
    first_name: "",
    last_name: "",
    email: "",
    phone: "",
    address: "",
    note: "",
    payment_mode: "",
  });

  const onChange = (value, field) => {
    setFields({ ...fields, [field]: value });
  };

  useEffect(() => {
    if (localStorage && localStorage.getItem("fields")) {
      setFields({ ...JSON.parse(localStorage.getItem("fields")), payment_mode: "" });
    }
    if (company && localStorage.getItem("orders")) {
      const orders = JSON.parse(localStorage.getItem("orders")).filter((o) => {
        return o.company_id === company.id;
      });
      const current_order = orders.pop();
      dispatch({ type: "init_orders", payload: { orders, current_order } });
    }
  }, [company]);

  useEffect(() => {
    setAllowToBuy(fields.payment_mode != "" && fields.last_name != "" && ((fields.phone != "" && validatePhoneNumber(fields.phone)) || (fields.email != "" && validateEmail(fields.email))));
  }, [fields]);

  useEffect(() => {
    if (current_order) {
      onClose();
      // redirect to payment_page
    }
  }, [current_order]);

  const makeOrder = () => {
    // save fields
    localStorage.setItem("fields", JSON.stringify(fields));

    // place the order
    const subtotal = productInCart.price * productInCart.quantity;
    const order = {
      client_email: fields.email,
      customer_details: fields,
      company_id: company.id,
      subtotal,
      shipping: company.shipping_fee || 0,
      total: subtotal + (company.shipping_fee || 0),
      note: fields.note,
      source: "Genuka Buy",
      payment: {
        date: new Date(),
        mode: fields.payment_mode,
        state: 0,
      },
      shipping: undefined,
      produits: [
        {
          id: productInCart.product.id,
          quantity: productInCart.quantity,
          price: productInCart.price,
          add_to_cart_date: new Date(),
          properties: productInCart.variants.map((v) => {
            return {
              [v.name]: v.options.map((o) => o.name),
            };
          }),
          complement: productInCart.complement,
          note: productInCart.note,
        },
      ],
    };
    placeOrder(dispatch, order);
  };

  if (!company) return <></>;

  const requiredVariantsSlug = productInCart.product.variants.filter(v => v.required).map(v => v.slug)
  const canOrder = productInCart.variants.length >= requiredVariantsSlug.length && productInCart.variants.filter(v => v.required).every((v) => requiredVariantsSlug.includes(v.slug) && v.options.length > 0);
  return (
    <>
      <Button ref={btnRef} onClick={onOpen} size="md" height="50px" width="100%" border="2px" colorScheme={"orange"} disabled={!canOrder} isLoading={!canOrder} spinner={<></>} loadingText="Sélectionnez les options obligatoires">
        PASSER MA COMMANDER
      </Button>
      <Drawer isOpen={isOpen} size="lg" placement="bottom" onClose={onClose} finalFocusRef={btnRef}>
        <DrawerOverlay />
        <DrawerContent>
          <Container maxW="2xl" maxH="100vh" style={{ overflow: "auto" }}>
            <DrawerCloseButton />
            <DrawerHeader>Finalisation de la commande</DrawerHeader>

            <DrawerBody>
              <Text fontSize="lg">👤 Informations générales</Text>
              <Divider />
              <br />
              <HStack>
                <FormControl mt="">
                  {/* <FormLabel>Prénom</FormLabel> */}
                  <Input
                    onChange={(e) => {
                      e.preventDefault();
                      onChange(e.target.value, "first_name");
                    }}
                    defaultValue={fields.first_name}
                    placeholder="Prénom"
                  />
                </FormControl>
                <FormControl mt="" isRequired>
                  {/* <FormLabel>Nom</FormLabel> */}
                  <Input
                    onChange={(e) => {
                      e.preventDefault();
                      onChange(e.target.value, "last_name");
                    }}
                    defaultValue={fields.last_name}
                    placeholder="Nom*"
                  />
                </FormControl>
              </HStack>
              <FormControl mt="2" isInvalid={fields.email != "" && !validateEmail(fields.email)}>
                {/* <FormLabel>Email</FormLabel> */}
                <InputGroup>
                  <InputLeftAddon children="📧" />
                  <Input
                    onChange={(e) => {
                      e.preventDefault();
                      onChange(e.target.value, "email");
                    }}
                    defaultValue={fields.email}
                    type="email"
                    placeholder="Email**"
                  />
                </InputGroup>
              </FormControl>
              <FormControl mt="2" isInvalid={fields.phone != "" && !validatePhoneNumber(fields.phone)}>
                {/* <FormLabel>Numéro de téléphone</FormLabel> */}
                <InputGroup>
                  <InputLeftAddon children="📞" />
                  <Input
                    type="tel"
                    onChange={(e) => {
                      e.preventDefault();
                      onChange(e.target.value, "phone");
                    }}
                    defaultValue={fields.phone}
                    placeholder="Numéro de téléphone**"
                  />
                </InputGroup>
              </FormControl>
              <FormControl mt="2">
                {/* <FormLabel>Adresse</FormLabel> */}
                <InputGroup>
                  <InputLeftAddon children="🏠" />
                  <Input
                    type="address"
                    onChange={(e) => {
                      e.preventDefault();
                      onChange(e.target.value, "address");
                    }}
                    defaultValue={fields.address}
                    placeholder="Adresse / Quartier"
                  />
                </InputGroup>
              </FormControl>
              <FormControl mt="2">
                {/* <FormLabel>Adresse</FormLabel> */}
                <Textarea
                  onChange={(e) => {
                    e.preventDefault();
                    onChange(e.target.value, "note");
                  }}
                  defaultValue={fields.note}
                  placeholder="🗒️ Note pour la commande"
                />
              </FormControl>
              <br />
              <small>(*) : Champ obligatoire</small>
              <br />
              <small>(**) : Au moins un des champs est obligatoire (Email ou numéro de téléphone)</small>
              <br />
              <br />
              <Text fontSize="lg">💰 Mode de paiement</Text>
              <Divider />
              <br />
              <RadioGroup name="payment_mode" defaultValue="cash" value={fields.payment_mode}>
                <VStack align={"self-start"}>
                  {company.payment_modes ? (
                    Object.keys(company.payment_modes)
                      .filter((key) => company.payment_modes[key].accept)
                      .filter((key) => key !== "paypal")
                      .map((key) => {
                        return (
                          <Radio
                            key={key}
                            value={key}
                            onChange={(e) => {
                              e.preventDefault();
                              onChange(e.target.value, "payment_mode");
                            }}
                          >
                            {company.payment_modes[key].full_name} {key === "cash" ? "(à la livraison)" : (key === "mobilemoney" ? "(Orange/MTN)" : "")}
                          </Radio>
                        );
                      })
                  ) : (
                    <Radio
                      value="cash"
                      onChange={(e) => {
                        e.preventDefault();
                        onChange(e.target.value, "payment_mode");
                      }}
                    >
                      Espèces à la livraison
                    </Radio>
                  )}
                </VStack>
              </RadioGroup>
              <br />

              {company.shipping_fee > 0 ? (
                <>
                  <br />
                  <Text fontSize="lg">🛵 Livraison</Text>
                  <Divider />
                  <br />
                  Frais de livraison :{" "}
                  <strong>
                    {new Intl.NumberFormat("en-US", {
                      style: "currency",
                      currency: company.currency.code,
                      maximumFractionDigits: 0,
                    }).format(company.shipping_fee)}
                  </strong>
                </>
              ) : (
                ""
              )}
              <br />
              <Divider my="5" />
              {error && <Error error_text={error} />}
            </DrawerBody>

            <DrawerFooter>
              <Button size="lg" variant="outline" mr={3} onClick={onClose} disabled={loading && loading.order}>
                Cancel
              </Button>
              <Button size="lg" colorScheme="yellow" disabled={!allowToBuy || (loading && loading.order)} isLoading={loading && loading.order} width="full" onClick={makeOrder}>
                VALIDER (
                {new Intl.NumberFormat("en-US", {
                  style: "currency",
                  currency: company.currency.code,
                  maximumFractionDigits: 0,
                }).format(productInCart.price * productInCart.quantity + company.shipping_fee)}
                ){/* <sup>{company.currency.symbol}</sup> */}
              </Button>
            </DrawerFooter>
          </Container>
        </DrawerContent>
      </Drawer>
    </>
  );
}

export default CheckoutDrawer;
